package tn.iit.gestchampjoueurservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tn.iit.gestchampjoueurservice.dto.PlayerRequest;
import tn.iit.gestchampjoueurservice.dto.PlayerResponse;
import tn.iit.gestchampjoueurservice.model.Player;
import tn.iit.gestchampjoueurservice.repository.PlayerRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PalyerService {

    private final PlayerRepository joueurRepository;
    public void createJoueur(PlayerRequest joueurRequest){

        Player joueur = Player.builder()
                .firstName(joueurRequest.getFirstName())
                .lastName(joueurRequest.getLastName())
                .nationality(joueurRequest.getNationality())
                .position(joueurRequest.getPosition())
                .build();

        joueurRepository.save(joueur);
        log.info("Joueur {} is saved",joueur.getId());
    }

    public List<PlayerResponse> getAllJoueur() {
        List<Player> joueurs= joueurRepository.findAll();
        return joueurs.stream().map(this::mapToJoueurResponse).toList();
    }

    private PlayerResponse mapToJoueurResponse(Player joueur) {
        return PlayerResponse.builder()
                .id(joueur.getId())
                .firstName(joueur.getFirstName())
                .lastName(joueur.getLastName())
                .nationality(joueur.getNationality())
                .position(joueur.getPosition())
                .build();
    }
}
