package tn.iit.gestchampjoueurservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestchampJoueurServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestchampJoueurServiceApplication.class, args);
	}

}
