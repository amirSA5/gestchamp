package tn.iit.gestchampjoueurservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.iit.gestchampjoueurservice.model.Player;

public interface PlayerRepository extends JpaRepository<Player, Integer> {
}
