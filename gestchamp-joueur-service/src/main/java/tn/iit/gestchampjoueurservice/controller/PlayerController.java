package tn.iit.gestchampjoueurservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import tn.iit.gestchampjoueurservice.dto.PlayerRequest;
import tn.iit.gestchampjoueurservice.dto.PlayerResponse;
import tn.iit.gestchampjoueurservice.service.PalyerService;

import java.util.List;

@RestController
@RequestMapping("/api/joueur")
@RequiredArgsConstructor
public class PlayerController {

    private final PalyerService joueurService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createJoueur(@RequestBody PlayerRequest joueurRequest){
        joueurService.createJoueur(joueurRequest);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<PlayerResponse> getAllJoueur(){
        return joueurService.getAllJoueur();
    }
}
