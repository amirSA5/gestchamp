package tn.iit.gestchampclassementservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestchampClassementServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestchampClassementServiceApplication.class, args);
    }

}
