package tn.iit.gestchampequipeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestchampEquipeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestchampEquipeServiceApplication.class, args);
    }

}
