package tn.iit.gestchampequipeservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tn.iit.gestchampequipeservice.dto.TeamLinePlayersDto;
import tn.iit.gestchampequipeservice.dto.TeamRequest;
import tn.iit.gestchampequipeservice.dto.TeamResponse;
import tn.iit.gestchampequipeservice.model.Team;
import tn.iit.gestchampequipeservice.model.TeamLinePlayers;
import tn.iit.gestchampequipeservice.repository.TeamRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TeamService {

    private final TeamRepository teamRepository;

    public void createTeam(TeamRequest teamRequest){
        Team team = new Team();

        List<TeamLinePlayers> list = teamRequest.getTeamLinePlayersDtos()
                .stream()
                .map(this::mapToDto)
                .toList();
        team.setTeamLinePlayers(list);

        teamRepository.save(team);

    }

    private TeamLinePlayers mapToDto(TeamLinePlayersDto teamLinePlayersDto) {
        return new TeamLinePlayers();
    }

    public List<TeamResponse> getAllTeams() {
        List<Team> teams= teamRepository.findAll();
        return teams.stream().map(this::mapToTeamsResponse).toList();
    }

    private TeamResponse mapToTeamsResponse(Team team) {
        return TeamResponse.builder()
                .id(team.getId())
                .name(team.getName())
                .trainer(team.getTrainer())
                .teamLinePlayers((List<TeamLinePlayers>) team.getTeamLinePlayers())
                .build();
    }
}
