package tn.iit.gestchampequipeservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "t_team_line_players")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TeamLinePlayers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
}
