package tn.iit.gestchampequipeservice.dto;

import jakarta.persistence.CascadeType;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tn.iit.gestchampequipeservice.model.TeamLinePlayers;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TeamResponse {
    private int id;
    private String name;
    private String trainer;
    private List<TeamLinePlayers> teamLinePlayers;
}
