package tn.iit.gestchampequipeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.iit.gestchampequipeservice.model.Team;

public interface TeamRepository extends JpaRepository<Team, Integer> {
}
