package tn.iit.gestchampequipeservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import tn.iit.gestchampequipeservice.dto.TeamRequest;
import tn.iit.gestchampequipeservice.dto.TeamResponse;
import tn.iit.gestchampequipeservice.service.TeamService;

import java.util.List;

@RestController
@RequestMapping("/api/teams")
@RequiredArgsConstructor
public class TeamController {

    private final TeamService teamService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String createTeam(@RequestBody TeamRequest teamRequest){
        teamService.createTeam(teamRequest);
        return "Team created sucessfuly";
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<TeamResponse> getAllTeams(){
        return teamService.getAllTeams();
    }
}
