package tn.iit.gestchampmatchservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tn.iit.gestchampmatchservice.dto.GameResponse;
import tn.iit.gestchampmatchservice.model.Game;
import tn.iit.gestchampmatchservice.repository.GameRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GameService {

    private final GameRepository gameRepository;

    public List<GameResponse> getAllGames() {
        List<Game> games= gameRepository.findAll();
        return games.stream().map(this::mapToGamesResponse).toList();
    }

    private GameResponse mapToGamesResponse(Game game) {
        return GameResponse.builder()
                .id(game.getId())
                .teamA(game.getTeamA())
                .teamB(game.getTeamB())
                .arbitrator(game.getArbitrator())
                .stadium(game.getStadium())
                .ability(game.getAbility())
                .matchDate(game.getMatchDate())
                .numberOfSpectators(game.getNumberOfSpectators())
                .scorersTeamA(game.getScorersTeamA())
                .scorersTeamB(game.getScorersTeamB())
                .build();
    }
}
