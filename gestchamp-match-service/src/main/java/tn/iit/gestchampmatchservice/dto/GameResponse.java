package tn.iit.gestchampmatchservice.dto;

import jakarta.persistence.CascadeType;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tn.iit.gestchampmatchservice.model.SCORERS;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GameResponse {

    private long id;
    private String teamA;
    private String teamB;
    private String arbitrator;
    private String stadium;
    private long ability;
    private Date matchDate;
    private long numberOfSpectators;
    private List<SCORERS> scorersTeamA;
    private List<SCORERS> scorersTeamB;
}
