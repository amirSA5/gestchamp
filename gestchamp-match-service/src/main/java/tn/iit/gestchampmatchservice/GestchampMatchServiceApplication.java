package tn.iit.gestchampmatchservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestchampMatchServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestchampMatchServiceApplication.class, args);
	}

}
