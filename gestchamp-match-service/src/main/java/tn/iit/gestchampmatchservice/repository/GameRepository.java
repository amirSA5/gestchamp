package tn.iit.gestchampmatchservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.iit.gestchampmatchservice.model.Game;

public interface GameRepository extends JpaRepository<Game, Long> {
}
