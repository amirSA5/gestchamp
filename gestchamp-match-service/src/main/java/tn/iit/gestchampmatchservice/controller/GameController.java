package tn.iit.gestchampmatchservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import tn.iit.gestchampmatchservice.dto.GameResponse;
import tn.iit.gestchampmatchservice.service.GameService;

import java.util.List;

@RestController
@RequestMapping("/api/game")
@RequiredArgsConstructor
public class GameController {

    private final GameService gameService;


    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<GameResponse> getGames(){
        return gameService.getAllGames();
    }
}
