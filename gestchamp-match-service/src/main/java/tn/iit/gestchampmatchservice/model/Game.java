package tn.iit.gestchampmatchservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="t_game")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String teamA;
    private String teamB;
    private String arbitrator;
    private String stadium;
    private long ability;
    private Date matchDate;
    private long numberOfSpectators;
    @OneToMany(cascade = CascadeType.ALL)
    private List<SCORERS> scorersTeamA;
    @OneToMany(cascade = CascadeType.ALL)
    private List<SCORERS> scorersTeamB;



}
