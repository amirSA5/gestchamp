package tn.iit.gestchampmatchservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="t_scorers")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SCORERS {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String firstname;
    private String lastname;
    private int miniScore;
}
